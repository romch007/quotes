import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema } from '@ioc:Adonis/Core/Validator'

import Quote from 'App/Models/Quote'

export default class QuotesController {
  public async index({ view }: HttpContextContract) {
    const quotes = await Quote.all()
    return view.render('home', { quotes })
  }

  public async create({ view }: HttpContextContract) {
    return view.render('new')
  }

  public async store({ request, response }: HttpContextContract) {
    const { text, description } = await request.validate({
      schema: schema.create({
        text: schema.string(),
        description: schema.string.optional(),
      }),
    })
    await Quote.create({ text, description })
    return response.redirect().toRoute('quotes.index')
  }
}
