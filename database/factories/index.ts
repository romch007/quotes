import Factory from '@ioc:Adonis/Lucid/Factory'
import Quote from 'App/Models/Quote'

export const QuoteFactory = Factory.define(Quote, ({ faker }) => ({
  text: faker.lorem.sentence(),
  description: faker.lorem.words(),
})).build()
