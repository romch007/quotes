import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import { QuoteFactory } from 'Database/factories'

export default class DevSeeder extends BaseSeeder {
  public async run() {
    await QuoteFactory.createMany(10)
  }
}
