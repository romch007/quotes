FROM node:lts-alpine AS builder

WORKDIR /builder

COPY package.json yarn.lock ./

RUN apk update && apk add git

RUN yarn install --frozen-lockfile

COPY . .

RUN node ace build --prod --client yarn

FROM node:lts-alpine

ENV NODE_ENV=production
ENV PORT=80
ENV HOST=0.0.0.0
ENV DB_CONNECTION=pg

WORKDIR /app

RUN apk update && apk add git

COPY --from=builder /builder/build/package.json /builder/build/yarn.lock ./

RUN yarn install --production --frozen-lockfile

COPY --from=builder /builder/build /app

CMD ["node", "server.js"]
